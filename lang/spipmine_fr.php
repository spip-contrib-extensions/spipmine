<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;


$GLOBALS[$GLOBALS['idx_lang']] = array(
	//A
	'ajouter_client'           => 'Ajouter un client',
	'ajouter_activite'         => 'Ajouter une activité',
	'aller_projet'             => 'Aller au projet',
	'aller_projet_num'         => 'Aller au projet N°',
	'attribuer_maquette_objet' => 'Attribuer une maquette à l\'objet',

	//C
	'creer_facture'            => 'Créer une facture',
	'choisir_benchmark_projet' => 'Choisir un benchmark pour le projet',
	'choisir_cdc_objet'        => 'Choisir un cahier des charges pour l\'objet',
	'choisir_cdev_objet'       => 'Choisir un cahier de développement pour l\'objet',
	'client_non_trouve'		   => 'Client non trouvé.',
	'commentaires'             => 'Commentaires',

	//E
	'enjeux_client'            => 'Enjeux pour le client',

	// F
	'faire_devis'              => 'Faire un devis',

	//L
	'label_factures_liees'     => 'Factures liées',

	//M
	'methode'                  => 'Méthodologie proposée',

	// N
	'nouveau_contact'          => 'Nouveau contact',
	'nouvelle_organisation'    => 'Nouvelle organisation',

	//O
	'objectifs'                => 'Objectifs à atteindre',

	//P
	'page_ajouter_activite'    => 'Ajouter une activité',
	'page_ajouter_client'      => 'Ajouter un client',
	'projet_precedent'         => 'Projet précédent',
	'projet_suivant'           => 'Projet suivant',

	// V
	'voir_benchmark_projet'    => 'Voir le benchmark sites du projet',
	'voir_cdev_num'            => 'Voir le cahier de développement N°',
	'voir_cdc_num'             => 'Voir le cahier des charges N°',
	'voir_clients'             => 'Voir tous les clients',
	'voir_en_ligne'            => 'Voir en ligne...',
	'voir_maquette_num'        => 'Voir la maquette N°',
	'voir_projet_num'          => 'Voir le projet N°',
);
