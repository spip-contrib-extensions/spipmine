<?php
/**
 * Plugin SPIPMine
 *
 * @plugin  SPIPMine
 * @license GPL (c) 2022
 * @author  Cyril Marion
 *
 * @package SPIP\spipmine\Administrations
 **/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Fonction d'installation du plugin et de mise à jour.
 * Vous pouvez :
 * - créer la structure SQL,
 * - insérer du pre-contenu,
 * - installer des valeurs de configuration,
 * - mettre à jour la structure SQL
 **/
function spipmine_upgrade($nom_meta_base_version, $version_cible) {
	$maj = array();

	$maj['create'] = array(array('spipmine_meta'));

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

/**
 * Fonction de désinstallation du plugin.
 * Vous devez :
 * - nettoyer toutes les données ajoutées par le plugin et son utilisation
 * - supprimer les tables et les champs créés par le plugin.
 **/
function spipmine_vider_tables($nom_meta_base_version) {

	effacer_meta($nom_meta_base_version);
}


/* Fonction pour créé les crons à une date et heure figer */
function spipmine_meta() {
	$coordonnees = lire_config('coordonnees/objets');
	ecrire_config("coordonnees/objets", array($coordonnees, 'spip_auteurs', 'spip_organisations'));
}
